import React from "react";

const Slides = ({ slides }) => {
  const [position, setPosition] = React.useState(0);

  return (
    <div>
      <div id="navigation" className="text-center">
        <button
          data-testid="button-restart"
          disabled={position === 0}
          onClick={() => setPosition(0)}
          className="small outlined"
        >
          Restart
        </button>
        <button
          data-testid="button-prev"
          disabled={position === 0}
          onClick={() => setPosition(position - 1)}
          className="small"
        >
          Prev
        </button>
        <button
          data-testid="button-next"
          onClick={() => setPosition(position + 1)}
          disabled={position === slides.length - 1}
          className="small"
        >
          Next
        </button>
      </div>
      <div id="slide" className="card text-center">
        <h1 data-testid="title">{slides[position]["title"]}</h1>
        <p data-testid="text">{slides[position]["text"]}</p>
      </div>
    </div>
  );
};

export default Slides;